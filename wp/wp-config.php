<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'database' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R,H4a:;`(`s12,O;/,w,[v|2=rRQiA`1#[fO=/e]4Qs+DK)Y0$20N;i2o)Z]~vqu' );
define( 'SECURE_AUTH_KEY',  'sf]d_h#wBS,d^$s7DzgX8q{`ergCo8}ih{_`=^Ht@!lfx~}.!L0Jk>wU,`Z1Eze@' );
define( 'LOGGED_IN_KEY',    '!|Vodn3sT1AH.7a}QjT}d?Qe&HW_X7yMBNk~v<Q39[:)KDy;@_>DIdC:GVh+ev{E' );
define( 'NONCE_KEY',        '{5`7_wdid7a+zd|?.ncB$nB+F-eV=PGojw]D{,VX?wiNMI}B{zIXkaJL3sA{Le?v' );
define( 'AUTH_SALT',        '*J/2u@W$x?c%.]Or5ClXH)sFwQ>!U!gi_P8k|B+y(w;|o#@%4&RHm|wn.|[20W8h' );
define( 'SECURE_AUTH_SALT', '%d_uWB+oXrfRL+8}!lmg^y83$J&lEbWlKb>#^$+CwgM-.A Ygs5*F81!1[O/KID>' );
define( 'LOGGED_IN_SALT',   'yJ,enXRs*,D.viiu>5~h^Qo5?cw+7uq;K^$YO>%a+hVb~tt4@QD3dM%8N`%)/_:{' );
define( 'NONCE_SALT',       'Ck,N{.Cx[WCN{;%a:hbI:i?/!HK|qx~Pcm]R<=Occ890A[^K9mTLPFEj~nzb]$b>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
